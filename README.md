tf-aws-redshift
-----

A Terraform module that creates an AWS Redshift cluster and Redshift subnet
group. You need to create the VPC, subnets, VPC SGs and any other resources
outside of this module.

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

Usage
-----

```js

module "redshift_cluster" {

  source = "git::ssh://git@gogs.bashton.net:Bashton/tf-aws-redshift.git"

  envname     = "dev"
  db_name     = "mydata"
  master_pass = "somethingsecret"
  vpc_sgs     = "vpc-sg"
  subnet_ids  = "vpc-subnet"

}
```

### Enabling logging to S3

Use the three associated variables in this module to enable logging to
S3 `logging_bucketname`, `logging_enable` and `logging_file_prefix`.

The bucket being written to will require the following policy. Where `AccountId` is the Redshift Service Account for a specific region (https://docs.aws.amazon.com/redshift/latest/mgmt/db-auditing.html), and `BucketName` is the name of your bucket.  

```js
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "Put bucket policy needed for audit logging",
			"Effect": "Allow",
			"Principal": {
				"AWS": "arn:aws:iam::AccountId:user/logs"
			},
			"Action": "s3:PutObject",
			"Resource": "arn:aws:s3:::BucketName/*"
		},
		{
			"Sid": "Get bucket policy needed for audit logging ",
			"Effect": "Allow",
			"Principal": {
				"AWS": "arn:aws:iam::AccountID:user/logs"
			},
			"Action": "s3:GetBucketAcl",
			"Resource": "arn:aws:s3:::BucketName"
		}
	]
} 
```


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allow_upgrade | If true, major version upgrades can be applied during the maintenance window to the Amazon Redshift engine that is running on the cluster | string | `true` | no |
| cluster_identifier | The Cluster Identifier. Must be a lower case string | string | - | yes |
| cluster_type | The cluster type to use, either single-node or multi-node | string | `single-node` | no |
| cluster_subnet_group_name | The name of a cluster subnet group to be associated with this cluster, applied if create_redshift_subnet_group is set to true | string | `` | no |
| create_redshift_subnet_group | Option to disable redshift subnet group creation. If disabled then cluster_subnet_group_name needs to be provided. Enabled by default. | string | `true` | no |
| db_name | The name of the first database to be created when the cluster is created | string | `dev` | no |
| enable_redshift_cluster | Option to enable or disable the redshift cluster. Enabled by default. | string | `true` | no |
| encrypted | Enables encryption at rest | string | `false` | no |
| enhanced_vpc_routing | Enhanced VPC Routing forces all COPY and UNLOAD traffic between your cluster and your data repositories through your Amazon VPC, not through the internet. Your VPC will require the necessary network routes between your clusters VPC and data resources. | string | `false` | no |
| envname | This will become the name suffix for resources created by this module | string | - | yes |
| final_snapshot_identifier | The identifier of the final snapshot that is to be created immediately before deleting the cluster | string | `final-snapshot` | no |
| kms_key_id | KMS encryption key ARN, required if encryption has been enabled | string | `` | no |
| logging_bucket_name | The S3 bucket which cluster logs should be written to if enabled | string | `` | no |
| logging_enable | Determines whether logging to S3 should be enabled on the cluster | string | `false` | no |
| logging_file_prefix | The prefix to apply to log files written to S3 | string | `` | no |
| maint_window | The weekly time range (UTC) during which automated cluster maintenance can occur | string | `Sun:02:30-Sun:04:30` | no |
| master_pass | Password for the master DB user | string | - | yes |
| master_user | Username for the master DB user | string | `root` | no |
| node_type | The instance type to be provisioned for the cluster | string | `dc1.large` | no |
| number_of_nodes | The number of compute nodes in the cluster | string | `1` | no |
| owner_account | The AWS customer account used to create or copy the snapshot. Required if you are restoring a snapshot you do not own, optional if you own the snapshot | string | `` | no |
| public | If true, the cluster can be accessed from a public network | string | `false` | no |
| skip_final_snapshot | Determines whether a final snapshot of the cluster is created before Amazon Redshift deletes the cluster | string | `false` | no |
| snapshot_identifier | The name of the snapshot from which to create the new cluster | string | `` | no |
| snapshot_retention | The number of days that automated snapshots are retained. If the value is 0, automated snapshots are disabled | string | `7` | no |
| subnet_ids | A list of subnet IDs to associate with the aws_redshift_subnet_group | list | `<list>` | no |
| vpc_sgs | A list of Virtual Private Cloud (VPC) security groups to be associated with the cluster | list | `<list>` | no |
| cluster_parameter_group_name | The name of the parameter group to be associated with the cluster | string | `` | no |
| iam_roles | (Optional) A list of IAM Role ARNs to associate with the cluster. A Maximum of 10 can be associated to the cluster at any time. | list | `` | no |
| enable_spectrum | (Optional) Adds a role with the required policies to enable use of the AWS Spectrum service. If enabled you will be only able to add a maximum of 9 roles to the cluster using the iam_roles parameter. | string | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster_az |  |
| cluster_endpoint |  |
| cluster_id |  |
| cluster_identifier |  |
| subnet_group_id |  |

