 ## 0.3.9 (April 24, 2018)

IMPROVEMENTS:
* Added Enhanced VPC Routing

BUG FIXES:
* Updated deprecated output resource attributes
* Updated output resource attributes to include splat 

## 0.0.8 (February 27, 2017)

BUG FIXES:
* Modified variable input for `database_name` so this can be controlled directly


## 0.0.7 (February 27, 2017)

BUG FIXES:
* Added variable for `owner_account` as this is required when utilising existing shared snapshots


## 0.0.6 (February 22, 2017)

IMPROVEMENTS:
* Updated Readme to include variables and outputs
* Added snapshot ID variable, and changed default for `skip_final_snapshot` to `False`

BUG FIXES:
* Added default for `final_snapshot_identifier` as a value for this is needed when creating a final snapshot


## 0.0.5 (February 08, 2017)

IMPROVEMENTS:
* Changed comma seperated list for `aws_redshift_subnet_group` to generic type `list`


## 0.0.4 (December 01, 2016)

IMPROVEMENTS:
* Added variable to choose the count of nodes in the Redshift cluster


## 0.0.3 (April 08, 2016)

IMPROVEMENTS:
* Added variable to allow changing redshift cluster to be public/private


## 0.0.2 (February 03, 2016)

Initial version

