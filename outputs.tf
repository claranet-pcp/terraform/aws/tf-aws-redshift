output "cluster_id" {
  value = join("", aws_redshift_cluster.cluster.*.id)
}

output "cluster_identifier" {
  value = join("", aws_redshift_cluster.cluster.*.cluster_identifier)
}

output "cluster_az" {
  value = join("", aws_redshift_cluster.cluster.*.availability_zone)
}

output "cluster_endpoint" {
  value = join("", aws_redshift_cluster.cluster.*.endpoint)
}

output "subnet_group_id" {
  value = join("", aws_redshift_subnet_group.subnet.*.id)
}

output "cluster_subnet_group_name" {
  value = join("", aws_redshift_cluster.cluster.*.cluster_subnet_group_name)
}
