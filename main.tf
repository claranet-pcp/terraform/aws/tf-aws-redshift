resource "aws_redshift_cluster" "cluster" {
  count                               = var.enable_redshift_cluster ? 1 : 0
  cluster_identifier                  = var.cluster_identifier
  database_name                       = var.db_name
  encrypted                           = var.encrypted
  enhanced_vpc_routing                = var.enhanced_vpc_routing
  kms_key_id                          = var.kms_key_id
  master_username                     = var.master_user
  master_password                     = var.master_pass
  node_type                           = var.node_type
  cluster_type                        = var.cluster_type
  vpc_security_group_ids              = var.vpc_sgs
  cluster_subnet_group_name           = var.create_redshift_subnet_group ? join("", aws_redshift_subnet_group.subnet.*.id) : var.cluster_subnet_group_name
  allow_version_upgrade               = var.allow_upgrade
  preferred_maintenance_window        = var.maint_window
  automated_snapshot_retention_period = var.snapshot_retention
  publicly_accessible                 = var.public
  number_of_nodes                     = var.number_of_nodes
  cluster_parameter_group_name        = var.cluster_parameter_group_name
  snapshot_identifier                 = var.snapshot_identifier
  owner_account                       = var.owner_account
  skip_final_snapshot                 = var.skip_final_snapshot
  final_snapshot_identifier           = var.final_snapshot_identifier
  iam_roles                           = concat(var.iam_roles, aws_iam_role.redshift-spectrum.*.arn)

  logging {
    enable        = var.logging_enable
    bucket_name   = var.logging_bucket_name
    s3_key_prefix = var.logging_file_prefix
  }
}

resource "aws_redshift_subnet_group" "subnet" {
  count       = var.create_redshift_subnet_group && var.enable_redshift_cluster ? 1 : 0
  name        = "redshift-${var.envname}"
  description = "Redshift Subnet Group ${var.envname}"
  subnet_ids  = var.subnet_ids
}

resource "aws_iam_role" "redshift-spectrum" {
  count = var.enable_spectrum && var.enable_redshift_cluster ? 1 : 0
  name  = "${var.cluster_identifier}-spectrum-role"

  assume_role_policy = data.aws_iam_policy_document.redshift_assume_role_policy[0].json
}

data "aws_iam_policy_document" "redshift_assume_role_policy" {
  count = var.enable_spectrum && var.enable_redshift_cluster ? 1 : 0

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["redshift.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "redshift_aws_policies" {
  count = var.enable_spectrum && var.enable_redshift_cluster ? 3 : 0

  role       = aws_iam_role.redshift-spectrum[0].id
  policy_arn = "arn:aws:iam::aws:policy/${list("AmazonS3ReadOnlyAccess", "AWSGlueConsoleFullAccess", "AmazonAthenaFullAccess")[count.index]}"

  lifecycle {
    create_before_destroy = true
  }
}
