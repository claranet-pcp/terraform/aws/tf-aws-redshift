/* Environment variables */
variable "envname" {
  description = "This will become the name suffix for resources created by this module"
  type        = string
}

/* Redshift variables */
variable "enable_redshift_cluster" {
  description = "Option to enable or disable the redshift cluster"
  type        = bool
  default     = true
}

variable "cluster_identifier" {
  description = "The Cluster Identifier. Must be a lower case string."
  type        = string
}

variable "db_name" {
  description = "The name of the first database to be created when the cluster is created"
  type        = string
  default     = "dev"
}

variable "master_user" {
  description = "Username for the master DB user"
  type        = string
  default     = "root"
}

variable "master_pass" {
  description = "Password for the master DB user"
  type        = string
}

variable "node_type" {
  description = "The instance type to be provisioned for the cluster"
  type        = string
  default     = "dc1.large"
}

variable "cluster_type" {
  description = "The cluster type to use, either single-node or multi-node"
  type        = string
  default     = "single-node"
}

variable "vpc_sgs" {
  description = "A list of Virtual Private Cloud (VPC) security groups to be associated with the cluster"
  type        = list(string)
  default     = []
}

variable "allow_upgrade" {
  description = "If true, major version upgrades can be applied during the maintenance window to the Amazon Redshift engine that is running on the cluster"
  type        = bool
  default     = true
}

variable "maint_window" {
  description = "The weekly time range (UTC) during which automated cluster maintenance can occur"
  type        = string
  default     = "Sun:02:30-Sun:04:30"
}

variable "snapshot_retention" {
  description = "The number of days that automated snapshots are retained. If the value is 0, automated snapshots are disabled"
  type        = string
  default     = "7"
}

variable "public" {
  description = "If true, the cluster can be accessed from a public network"
  type        = bool
  default     = false
}

variable "number_of_nodes" {
  description = "The number of compute nodes in the cluster"
  type        = string
  default     = "1"
}

variable "snapshot_identifier" {
  description = "The name of the snapshot from which to create the new cluster"
  type        = string
  default     = ""
}

variable "owner_account" {
  description = "The AWS customer account used to create or copy the snapshot. Required if you are restoring a snapshot you do not own, optional if you own the snapshot"
  type        = string
  default     = ""
}

variable "skip_final_snapshot" {
  description = "Determines whether a final snapshot of the cluster is created before Amazon Redshift deletes the cluster"
  type        = bool
  default     = false
}

variable "final_snapshot_identifier" {
  description = "The identifier of the final snapshot that is to be created immediately before deleting the cluster"
  type        = string
  default     = "final-snapshot"
}

variable "subnet_ids" {
  description = "A list of subnet IDs to associate with the aws_redshift_subnet_group"
  type        = list(string)
  default     = []
}

variable "logging_enable" {
  description = "Determines whether logging to S3 should be enabled on the cluster"
  type        = bool
  default     = false
}

variable "logging_bucket_name" {
  description = "The S3 bucket which cluster logs should be written to if enabled"
  type        = string
  default     = ""
}

variable "logging_file_prefix" {
  description = "The prefix to apply to log files written to S3"
  type        = string
  default     = ""
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying kms_key_id, encrypted needs to be set to true."
  type        = string
  default     = ""
}

variable "encrypted" {
  description = "If true, the data in the cluster is encrypted at rest."
  type        = bool
  default     = false
}

variable "enhanced_vpc_routing" {
  description = "If true, enhanced VPC routing is enabled"
  type        = bool
  default     = false
}

variable "cluster_parameter_group_name" {
  description = "The name of the parameter group to be associated with this cluster."
  type        = string
  default     = ""
}

variable "iam_roles" {
  description = "(Optional) A list of IAM Role ARNs to associate with the cluster. A Maximum of 10 can be associated to the cluster at any time."
  type        = list(string)
  default     = []
}

variable "enable_spectrum" {
  description = "(Optional) Adds a role with the required policies to enable use of the AWS Spectrum service."
  type        = bool
  default     = false
}

variable "cluster_subnet_group_name" {
  description = "The name of a cluster subnet group to be associated with this cluster. Will only be applied if create_redshift_subnet_group is set to false"
  type        = string
  default     = ""
}

variable "create_redshift_subnet_group" {
  description = "Option to disable the redshift subnet group created by the module. If disabled, please specify cluster_subnet_group_name or cluster will be deployed outside VPC"
  type        = bool
  default     = true
}
